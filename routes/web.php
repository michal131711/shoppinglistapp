<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::middleware('web')->group(function () {
    Route::controller(UserController::class)->group(function () {
//        Route::post('login', 'login');
//        Route::post('logout', 'logout');
//        Route::post('refresh', 'refresh');
//        Route::post('me', 'me');
        Route::get('users', 'index')->name('list.users');
        Route::post('users/sort', 'sort')->name('list.sortdate');
    });
});
