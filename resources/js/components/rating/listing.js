import React from 'react';
import {ApolloClient, gql, InMemoryCache} from '@apollo/client';

const endpoint = "http://shoppinglistapp.test/graphql/";
const GET_USERS = gql`
    {
        users {
            id
            email
            rating
            name
            comment
            photo_url
            created_at
            updated_at
        }
    }
`;
const GET_SORTED_USERS = gql`
    query GetSortedUsers($nameSort: String!, $typeSort: String!) {
        getSortedUsers(orderBy: { field: $nameSort, direction: $typeSort}) {
            id
            email
            rating
            name
            comment
            photo_url
            created_at
            updated_at
        }
    }
`;

export class ListingRating extends React.Component {
    client = new ApolloClient({
        uri: endpoint,
        cache: new InMemoryCache(),
    });

    constructor(props) {
        super(props);
        this.state = {
            users: [],
            name: '',
            email: '',
        };
    }

    getListUsers = async () => {
        try {
            const { data } = await this.client.query({
                query: GET_USERS,
            });

            this.setState({ users: data.users });
        } catch (error) {
            console.error(error);
        }
    };

    componentDidMount() {
        this.getListUsers();
    }

    sortDate = async (nameSort, typeSort) => {
        try {  const {data} = await this.client.query({
            query: GET_SORTED_USERS,
            variables: { nameSort, typeSort },
        });
        const sortedUsers = data.getSortedUsers;
        console.log(sortedUsers)
        this.setState({
            users: sortedUsers,
        });

        } catch (error) {
            console.error(error);
        }
    }

    handleInputChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };

    render() {
        const { users } = this.state;

        return <div className="panel panel-default">
            <div className="panel-heading">Sortable</div>
            <div className="panel-body mb-3">
                <div className="btn-group">
                    <button type="button" onClick={() => this.sortDate('created_at', 'ASC')}
                            className="btn btn-primary mr-3">
                        Date asc
                    </button>
                    <button type="button" onClick={() => this.sortDate('created_at', 'DESC')}
                            className="btn btn-primary mr-3">
                        Date desc
                    </button>
                    <button type="button" onClick={() => this.sortDate('rating', 'ASC')}
                            className="btn btn-primary ml-3">
                        Rating asc
                    </button>
                    <button type="button" onClick={() => this.sortDate('rating', 'DESC')}
                            className="btn btn-primary ml-3">
                        Rating desc
                    </button>
                </div>
            </div>
            <ol className="list-group list-group-numbered">
                {users.map(item => (
                    <li className="list-group-item d-flex justify-content-between align-items-start" key={item.id}>
                        <div className="ms-2 me-auto">
                            <img src={item.photo_url} alt={item.name} className="img-fluid" width={"100px"}/>
                            <div className="fw-bold">{item.name}</div>
                            Rating: {item.rating}
                        </div>
                        <span className="rounded-pill">
                                 <button type="button" className="btn btn-sm btn-danger">X</button>
                             </span>
                    </li>))}
            </ol>
        </div>;
    }
}
