import React from 'react';

export class ListingOrder extends React.Component {
    startIndex = 1;
    fruitsList = [{
        id: 1, name: 'Banana', quantity: 15,
    }, {
        id: 2, name: 'Apple', quantity: 13,
    }, {
        id: 3, name: 'Egg', quantity: 13,
    }];

    constructor(props) {
        super(props);
        this.state = {
            fruitsList: this.fruitsList
        }
    }
    normalizeName = (name) => {
        return name
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/[^a-zA-Z0-9\s]/g, "");
    }

    incrementOrDecrementQuantityFruit = (item, symbol) => {
        let getFindData = this.state.fruitsList.find(res => res.id === item.id);
        let getSumQuantity = symbol === '-' ? getFindData.quantity - this.startIndex : getFindData.quantity + this.startIndex;
        let findIndex = this.state.fruitsList.findIndex(el => el.id === item.id);

        this.state.fruitsList[findIndex] = {
            id: getFindData.id,
            name: this.normalizeName(getFindData.name.trim()),
            quantity: getSumQuantity,
        }

        const filteredFruitsList = this.state.fruitsList.filter((value) => value.quantity >= 1);

        this.setState({
            fruitsList: filteredFruitsList
        });
    }

    render() {
        return <div className="panel panel-default">
            <ol className="list-group list-group-numbered">
                {this.state.fruitsList.map(item => (
                    <li className="list-group-item d-flex justify-content-between align-items-start" key={item.id}>
                        <div className="ms-2 me-auto">
                            <div className="fw-bold">{item.name}</div>
                        </div>
                        <span className="rounded-pill mr-3">
                            <button type="button" onClick={event => this.incrementOrDecrementQuantityFruit(item, '-')}
                                    className="btn btn-sm btn-danger">-</button>
                        </span>
                        quantity: {item.quantity}
                        <span className="rounded-pill ml-2">
                            <button type="button" onClick={event => this.incrementOrDecrementQuantityFruit(item, '+')}
                                    className="btn btn-sm btn-danger">+</button>
                        </span>
                    </li>))}
            </ol>
        </div>;
    }
}
