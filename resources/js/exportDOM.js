import ReactDOM from "react-dom";
import React from "react";
import {ListingRating} from "./components/rating/listing";
import {ListingOrder} from "./components/shopping/orders";

const domNameArray = [
    {name: 'ratingListing', tagHTml: <ListingRating/>},
    {name: 'ordersListing', tagHTml: <ListingOrder/>},
];

domNameArray.map(value => {
    const domObject = document.getElementById(value.name);

    if (domObject) {
        return ReactDOM.render(value.tagHTml, domObject);
    }
});
