<?php

namespace App\services;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class UserService
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection|User[]
     */
    public function getAllUsers()
    {
        return $this->user->all();
    }

    /**
     * @param string $nameSort
     * @return object
     */
    public function getAllUsersSortedBy(string $nameSort, string $typeSort)
    {
        return $this->user->orderBy($nameSort,$typeSort)->get();
    }
}
