<?php

namespace App\GraphQL\Queries;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use LaravelIdea\Helper\App\Models\_IH_User_C;
use LaravelIdea\Helper\App\Models\_IH_User_QB;
use Nuwave\Lighthouse\Execution\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

final class GetSortedUsers
{
    /**
     * @param $root
     * @param array{} $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return _IH_User_C|_IH_User_QB[]|Builder[]|Collection|User[]
     */
    public function __invoke($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $orderBy = $args['orderBy'] ?? null;

        $query = User::query();
        // Apply sorting based on the orderBy argument
        if ($orderBy) {
            $field = $orderBy['field'];
            $direction = $orderBy['direction'];

            // Ensure the field is valid and can be sorted
            if (in_array($field, ['id', 'name', 'rating', 'email', 'email_verified_at', 'comment', 'photo_url', 'created_at', 'updated_at'])) {
                $query->orderBy($field, $direction);
            }
        }

        // Fetch the sorted users
        $users = $query->get();

        return $users;
    }
}
