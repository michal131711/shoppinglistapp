<?php

namespace App\GraphQL\Mutations;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class AuthMutation
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext $context
     * @param ResolveInfo $resolveInfo
     * @return JsonResponse
     * @throws Exception
     */
    public function customlogin($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): JsonResponse
    {
        $credentials = [
            'email' => $args['email'],
            'password' => $args['password'],
        ];

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json([
            'access_token' => $token,
        ], 200);
    }
}
