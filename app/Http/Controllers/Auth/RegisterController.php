<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Contracts\Validation\Validator as ValidatorAlias;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return ValidatorAlias
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'rating' => ['required', 'integer', 'max:30'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'comment' => ['required', 'string', 'max:255'],
            'photo_url' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param $data
     * @return User
     */
    protected function create($data)
    {
        $photo_url = null;

        if ($data['photo_url'] && $data['photo_url'] instanceof UploadedFile && $data['photo_url']->isValid()) {
            $photo_url = $data['photo_url']->store('public/photos');
            $photo_url = str_replace('public/', '/storage/', $photo_url);
        }

        return User::create([
            'name' => $data['name'],
            'rating' => $data['rating'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'comment' => $data['comment'],
            'photo_url' => $photo_url,
        ]);
    }
}
