<?php

namespace App\Providers;

use App\GraphQL\Queries\GetSortedUsers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->app->extend('lighthouse.resolvers', function ($resolvers, $app) {
            $resolvers['Query']['getSortedUsers'] = $app->make(GetSortedUsers::class).'@getSortedUsers';

            return $resolvers;
        });
    }
}
